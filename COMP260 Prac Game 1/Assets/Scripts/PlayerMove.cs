﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	public float maxSpeed = 5.0f;
	public string horizontalAxis = "Horizontal P1";
	public string verticalAxis = "Vertical P1";

	void Update() {
		// get the input values
		Vector2 direction;
		direction.x = Input.GetAxis(horizontalAxis);
		direction.y = Input.GetAxis(verticalAxis);

		// scale by the maxSpeed parameter
		Vector2 velocity = direction * maxSpeed;

		// move the object
		transform.Translate(velocity * Time.deltaTime);

	}
}
